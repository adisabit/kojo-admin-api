<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Request extends Model
{
    use SoftDeletes;

    /**
     * Expenditure request category constant
     */
    const CATEGORY_EXPENDITURE = 'EXPENDITURE';

    /**
     * Purchase request category constant
     */
    const CATEGORY_PURCHASE = 'PURCHASE';

    /**
     * Payroll request category constant
     */
    const CATEGORY_PAYROLL = 'PAYROLL';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * One to many relationship to request detail
     *
     * @return void
     */
    public function details()
    {
        return $this->hasMany(RequestDetail::class, 'request_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
