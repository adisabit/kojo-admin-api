<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Helper\JobSheetHelper;
use App\Library\Response;
use App\Model\JobSheet;
use App\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobSheetController extends Controller
{
    /**
     * Create job sheet
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        DB::beginTransaction();

        try {
            $details = $request->details;

            $code = JobSheetHelper::generateCode($request->batch);

            $jobSheet = JobSheet::create(array_merge($request->except('details'), [
                'code' => $code
            ]));

            foreach ($details as $detail) {
                Order::where('id', $detail['order_id'])->update([
                    'status' => $detail['status']
                ]);

                $jobSheet->details()->create($detail);
            }

            $jobSheet->load('details');

            DB::commit();

            return Response::instance()
                ->json($jobSheet)
                ->success(201);
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Get list of job sheet
     *
     * @return void
     */
    public function list()
    {
        $jobSheets = JobSheet::orderBy('created_at', 'desc')->get();

        return Response::instance()
            ->json($jobSheets)
            ->success();
    }

    /**
     * Retrieve job sheet
     *
     * @param mixed $id
     * @return void
     */
    public function retrieve($id)
    {
        $jobSheet = JobSheet::with('details')->where('id', $id)->first();

        if (!$jobSheet) {
            throw new DataNotFoundException('Job sheet tidak ditemukan!', 204);
        }

        $detailResult = [];

        $status = [
            'MATERIALIZING'     => 'Bahan',
            'CUTTING'           => 'Potong',
            'SEWING'            => 'Jahit',
            'SCREEN_PRINTING'   => 'Sablon',
            'EMBROIDERING'      => 'Bordir',
            'BUTTONING'         => 'Kancing',
            'PIPING'            => 'Obras',
            'PACKING'           => 'Packing',
        ];

        foreach ($jobSheet->details as $detail) {
            $order = Order::where('id', $detail['order_id'])->first();
            $detail['order_ref'] = $order->order_ref;
            $detail['status'] = $status[$order->status];

            $detailResult[] = $detail;
        }

        $jobSheet->details = $detailResult;

        return Response::instance()
            ->json($jobSheet)
            ->success();
    }

    /**
     * Retrieve processed orders
     *
     * @return void
     */
    public function retrieveOrders()
    {
        $orders = Order::where('status', Order::STATUS_PROCESSED)->get();

        return Response::instance()->json($orders)->success();
    }

    /**
     * Update job sheet
     *
     * @param Request $request
     * @param mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $jobSheet = JobSheet::with('details')->where('id', $id)->first();

        if (!$jobSheet) {
            throw new DataNotFoundException('Job sheet tidak ditemukan!', 204);
        }

        DB::beginTransaction();

        try {
            $details = $request->details;

            $result = $jobSheet->update($request->except('details'));

            foreach ($details as $detail) {
                $order = Order::where('id', $detail['order_id'])->first();

                if ($order) {
                    $order->update([
                        'status' => $detail['status'],
                        'remark' => $detail['remark']
                    ]);
                }

                $jobSheet->details()->update($detail);
            }

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Delete job sheet
     *
     * @param mixed $id
     * @return void
     */
    public function delete($id)
    {
        $jobSheet = JobSheet::with('details')->where('id', $id)->first();

        if (!$jobSheet) {
            throw new DataNotFoundException('Job sheet tidak ditemukan!', 204);
        }

        DB::beginTransaction();

        try {
            $jobSheet->details()->delete();

            $result = $jobSheet->delete();

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Delete detail
     *
     * @param $id
     * @param $detailId
     * @return void
     */
    public function deleteDetail($id, $detailId)
    {
        $jobSheet = JobSheet::with('details')->where('id', $id)->first();

        if (!$jobSheet) {
            throw new DataNotFoundException('Job sheet tidak ditemukan!', 204);
        }

        $jobSheetDetail = $jobSheet->details()->where('id', $detailId)->first();

        if (!$jobSheetDetail) {
            throw new DataNotFoundException('Job sheet detail tidak ditemukan!', 204);
        }

        $result = $jobSheetDetail->delete();

        return Response::instance()
            ->json($result)
            ->success();
    }
}
