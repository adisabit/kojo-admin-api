<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use Illuminate\Http\Request;
use App\Model\ClothingPuring;

class ClothingPuringController extends Controller
{
    /**
     * Get all clothing materials
     *
     * @return mixed
     */
    public function list()
    {
        $materials = ClothingPuring::orderBy('name')->get();

        return Response::instance()
            ->json($materials)
            ->success();
    }

    /**
     * Get clothing material detail
     *
     * @param int $id
     * @return mixed
     */
    public function detail($id)
    {
        $material = ClothingPuring::where('id', $id)->first();

        if (!$material) {
            throw new DataNotFoundException('Puring tidak ditemukan!');
        }

        return Response::instance()
            ->json($material)
            ->success();
    }

    /**
     * Delete materials
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $material = ClothingPuring::where('id', $id)->delete();

        return Response::instance()
            ->json(['total_deleted' => $material])
            ->success();
    }

    /**
     * Update materials
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $material = ClothingPuring::where('id', $request->id)
            ->update($request->all());

        return Response::instance()
            ->json(['total_updated' => $material])
            ->success();
    }

    /**
     * Create materials
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $material = ClothingPuring::create($request->all());

        return Response::instance()
            ->json($material)
            ->success(201);
    }
}
