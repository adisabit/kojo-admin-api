<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use Illuminate\Http\Request;
use App\Model\ClothingMaterial;

class ClothingMaterialController extends Controller
{
    /**
     * Get all clothing materials
     *
     * @return mixed
     */
    public function list()
    {
        $materials = ClothingMaterial::orderBy('name')->get();

        return Response::instance()
            ->json($materials)
            ->success();
    }

    /**
     * Get clothing material detail
     *
     * @param int $id
     * @return mixed
     */
    public function detail($id)
    {
        $material = ClothingMaterial::where('id', $id)->first();

        if (!$material) {
            throw new DataNotFoundException('Bahan tidak ditemukan!');
        }

        return Response::instance()
            ->json($material)
            ->success();
    }

    /**
     * Delete materials
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $material = ClothingMaterial::where('id', $id)->delete();

        return Response::instance()
            ->json(['total_deleted' => $material])
            ->success();
    }

    /**
     * Update materials
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $material = ClothingMaterial::where('id', $request->id)
            ->update($request->all());

        return Response::instance()
            ->json(['total_updated' => $material])
            ->success();
    }

    /**
     * Create materials
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $material = ClothingMaterial::create($request->all());

        return Response::instance()
            ->json($material)
            ->success(201);
    }
}
