<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use App\Model\Configuration;
use App\Model\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Spipu\Html2Pdf\Html2Pdf;

class InvoiceController extends Controller
{
    /**
     * Get list of invlices
     *
     * @return void
     */
    public function list()
    {
        $invoices = Invoice::orderBy('created_at', 'DESC')->get();

        foreach ($invoices as $k => $invoice) {
            $invoices[$k]['formatted_unit_price'] = formatIdr($invoice['unit_price']);
            $invoices[$k]['formatted_total_price'] = formatIdr($invoice['total_price']);
        }

        return Response::instance()
            ->json($invoices)
            ->success();
    }

    /**
     * Get invoice detail
     *
     * @return void
     */
    public function get($id)
    {
        $invoice = Invoice::where('id', $id)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $invoice['formatted_unit_price'] = formatIdr($invoice['unit_price']);
        $invoice['formatted_total_price'] = formatIdr($invoice['total_price']);
        $invoice['formatted_delivery_fee'] = formatIdr($invoice['delivery_fee']);

        $details = json_decode($invoice['detail'], true);

        if (!$details) {
            $details = [];
        }

        foreach ($details as $k => $detail) {
            $details[$k]['formatted_price'] = formatIdr($detail['price']);
        }

        $invoice['detail'] = $details;

        return Response::instance()
            ->json($invoice)
            ->success();
    }

    /**
     * Save invoice
     *
     * @param Request $request
     * @return void
     */
    public function save(Request $request)
    {
        $invoice = Invoice::where('order_code', $request->order_code)->first();

        if (!$invoice) {
            $request->merge([
                'code' => generateInvoiceCode()
            ]);

            $invoice = Invoice::create($request->all());
        } else {
            $invoice->update($request->all());
            $invoice = Invoice::where('order_code', $request->order_code)->first();
        }

        return Response::instance()
            ->json($invoice)
            ->success();
    }

    /**
     * Delete invoice
     *
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $invoice = Invoice::where('id', $id)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $result = $invoice->delete();

        return Response::instance()
            ->json($result)
            ->success();
    }

    public function download($id)
    {
        $invoice = Invoice::where('id', $id)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $invoiceHeader = Configuration::where('key', 'INVOICE_HEADER')->first();
        $invoiceHeader = $invoiceHeader->current_value ?? $invoiceHeader->default_value;

        $invoiceBody = Configuration::where('key', 'INVOICE_BODY')->first();
        $invoiceBody = $invoiceBody->current_value ?? $invoiceBody->default_value;

        $invoiceFooter = Configuration::where('key', 'INVOICE_FOOTER')->first();
        $invoiceFooter = $invoiceFooter->current_value ?? $invoiceFooter->default_value;


        $invoiceHeader = str_replace('{{ code }}', $invoice->code, $invoiceHeader);
        $invoiceHeader = str_replace('{{ name }}', $invoice->name, $invoiceHeader);
        $invoiceHeader = str_replace('{{ order_status }}', $invoice->order_status, $invoiceHeader);
        $invoiceHeader = str_replace('{{ institution }}', $invoice->institution, $invoiceHeader);
        $invoiceHeader = str_replace('{{ enter_date }}', $invoice->enter_date, $invoiceHeader);
        $invoiceHeader = str_replace('{{ address }}', $invoice->address, $invoiceHeader);
        $invoiceHeader = str_replace('{{ exit_date }}', $invoice->exit_date, $invoiceHeader);
        $invoiceHeader = str_replace('{{ phone_number }}', $invoice->phone_number, $invoiceHeader);
        $invoiceHeader = str_replace('{{ payment_method }}', $invoice->payment_method, $invoiceHeader);

        $details = json_decode($invoice->detail);

        $invoiceBodyResult = '';

        foreach ($details as $k => $detail) {
            $invoiceBodyTmp = $invoiceBody;

            $invoiceBodyTmp = str_replace('{{ index }}', $k + 1, $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ type }}', $detail->type, $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ material }}', $detail->material, $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ sleeve }}', $detail->sleeve, $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ size }}', $detail->size, $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ quantity }}', $detail->quantity, $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ price }}', formatIdr($detail->quantity), $invoiceBodyTmp);
            $invoiceBodyTmp = str_replace('{{ total_price }}', formatIdr($detail->quantity * $detail->price), $invoiceBodyTmp);

            $invoiceBodyResult .= $invoiceBodyTmp;
        }

        $invoiceFooter = str_replace('{{ price }}', formatIdr($invoice->price), $invoiceFooter);
        $invoiceFooter = str_replace('{{ delivery_fee }}', formatIdr($invoice->delivery_fee), $invoiceFooter);
        $invoiceFooter = str_replace('{{ total_price }}', formatIdr($invoice->total_price), $invoiceFooter);
        $invoiceFooter = str_replace('{{ is_paid_off }}', $invoice->is_paid_off == 1 ? 'LUNAS' : 'TIDAK', $invoiceFooter);

        $invoiceHtml = $invoiceHeader . $invoiceBodyResult . $invoiceFooter;

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($invoiceHtml);
        $dompdf->render();

        return $dompdf->stream();
    }

    public function downloadNew($id)
    {
        $invoice = Invoice::where('id', $id)->first();

        if (!$invoice) {
            throw new DataNotFoundException('Invoice tidak ditemukan!');
        }

        $invoice->detail = json_decode($invoice->detail);

        // return view('invoice/index', compact('invoice'));
        $view = View::make('invoice.index', compact('invoice'))->render();

        // echo $view;die;
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml($view, 'utf-8');
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        return $dompdf->stream("{$invoice->code}.pdf");
    }
}
