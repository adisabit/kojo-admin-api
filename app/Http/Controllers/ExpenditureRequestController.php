<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Helper\RequestHelper;
use App\Library\Response;
use App\Model\Request as ModelRequest;
use App\Model\RequestDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpenditureRequestController extends Controller
{
    /**
     * Create new expenditure request
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        DB::beginTransaction();

        // Genereate expenditure request code
        $expenditureRequestCode = RequestHelper::generateExpenditureRequestCode($request->date);

        try {
            // Create expenditure request
            $expenditureRequest = ModelRequest::create([
                'category'      => ModelRequest::CATEGORY_EXPENDITURE,
                'request_code'  => $expenditureRequestCode,
                'date'          => $request->date ?? date("Y-m-d H:i:s")
            ]);

            // Create expenditure request detail
            foreach ($request->details as $detail) {
                $data = array_merge($detail, [
                    'price_per_unit' => (int) $detail['total_price'] / (int) $detail['amount']
                ]);

                $expenditureRequest->details()->create($data);
            }

            DB::commit();

            $expenditureRequest->load('details');

            return Response::instance()
                ->json($expenditureRequest)
                ->success(201);

        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error();
        }
    }

    /**
     * Retrieve list of purchase request
     *
     * @return void
     */
    public function list()
    {
        $expenditureRequest = ModelRequest::with('details')
            ->where([
                'category'  => ModelRequest::CATEGORY_EXPENDITURE
            ])->get();

        return Response::instance()
            ->json($expenditureRequest)
            ->success();
    }

    /**
     * Retrieve expenditure request
     *
     * @param mixed $id
     * @return void
     */
    public function retrieve($id)
    {
        $expenditureRequest = ModelRequest::with('details')
            ->where([
                'id'        => $id,
                'category'  => ModelRequest::CATEGORY_EXPENDITURE
            ])
            ->first();

        if (!$expenditureRequest) {
            throw new DataNotFoundException('Expenditure request tidak ditemukan!');
        }

        return Response::instance()
            ->json($expenditureRequest)
            ->success();
    }

    /**
     * Update expenditure request
     *
     * @param Request $request
     * @param mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $expenditureRequest = ModelRequest::where([
            'id'        => $id,
            'category'  => ModelRequest::CATEGORY_EXPENDITURE
        ])->first();

        if (!$expenditureRequest) {
            throw new DataNotFoundException('Expenditure request tidak ditemukan!');
        }

        DB::beginTransaction();

        // Genereate expenditure request code
        $expenditureRequestCode = RequestHelper::generateExpenditureRequestCode($request->date);

        try {
            // Update expenditure request
            $result = $expenditureRequest->update([
                'date'          => $request->date ?? date("Y-m-d H:i:s"),
                'request_code'  => $expenditureRequestCode
            ]);

            // Update expenditure request details
            foreach ($request->details as $detail) {
                $data = [
                    'material'          => $detail['material'],
                    'amount'            => $detail['amount'],
                    'vendor'            => $detail['vendor'],
                    'unit'              => $detail['unit'],
                    'total_price'       => $detail['total_price'],
                    'price_per_unit'    => (int) $detail['total_price'] / (int) $detail['amount']
                ];

                RequestDetail::where('id', $detail['id'])->update($data);
            }

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error();
        }
    }

    /**
     * Delete expenditure request
     *
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $expenditureRequest = ModelRequest::with('details')
            ->where([
                'id'        => $id,
                'category'  => ModelRequest::CATEGORY_EXPENDITURE
            ])
            ->first();

        if (!$expenditureRequest) {
            throw new DataNotFoundException('Expenditure request tidak ditemukan!');
        }

        DB::beginTransaction();

        try {
            // Delete expenditure request details
            $expenditureRequest->details()->delete();

            // Update expenditure request
            $result = $expenditureRequest->delete();

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error();
        }
    }
}
