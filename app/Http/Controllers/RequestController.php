<?php

namespace App\Http\Controllers;

use App\Library\Response;
use App\Model\Order;
use App\Model\PayrollRequest;
use App\Model\Request as ModelRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    public function list()
    {
        $requests = ModelRequest::distinct()
            ->orderBy('created_at', 'desc')
            ->get(['order_id']);

        $result = [];

        foreach ($requests as $request) {
            $result[] = [
                'id'      => $request->order_id,
                'order_ref'     => $request->order->order_ref,
                'order_code'    => $request->order->order_code,
                'order_name'    => $request->order->order_name
            ];
        }

        return Response::instance()
            ->json($result)
            ->success();
    }

    public function detail($orderId)
    {
        $requests = ModelRequest::where('order_id', $orderId)->get()->toArray();

        $purchase = array_filter($requests, function($request) {
            return $request['category'] == ModelRequest::CATEGORY_PURCHASE;
        });

        $expenditure = array_filter($requests, function($request) {
            return $request['category'] == ModelRequest::CATEGORY_EXPENDITURE;
        });

        $payroll = array_filter($requests, function($request) {
            return $request['category'] == ModelRequest::CATEGORY_PAYROLL;
        });

        return Response::instance()
            ->json([
                'purchase_request'      => array_values($purchase),
                'expenditure_request'   => array_values($expenditure),
                'payroll_request'       => array_values($payroll),
            ])->success();
    }

    public function getOrders(Request $request)
    {
        $orderId = $request->order_id;
        
        if (!$orderId) {
            $orders = Order::whereNotIn('id', function($query) {
                $requestTable = with(new ModelRequest())->getTable();
    
                $query->select(['order_id'])
                    ->from($requestTable)
                    ->distinct()
                    ->get();
            })->get();
        } else {
            $orders = Order::where('id', $orderId)->get();
        }

        return Response::instance()
            ->json($orders)
            ->success();
    }

    /**
     * Get list of id
     *
     * @param $id
     * @param $request
     * @param $name
     * @return void
     */
    private function getIdList($id, $request, $name)
    {
        foreach ($request as $item) {
            if ($item['id'] != null) {
                $id[$name][] = $item['id'];
            }
        }

        return $id;
    }

    /**
     * Delete request
     *
     * @param $id
     * @param $model
     * @return void
     */
    private function deleteRequest($id, $orderId, $model, $category = NULL)
    {
        if ($category) {
            return $model::where([
                'order_id' => $orderId,
                'category' => $category
            ])->whereNotIn('id', $id)->delete();
        }

        return $model::whereNotIn('id', $id)->delete();
    }

    /**
     * Upsert
     *
     * @param $data
     * @param $model
     * @return array
     */
    private function upsert($data, $model)
    {
        $result = [];

        foreach ($data as $item) {
            if (!isset($item['total_price'])) {
                $item['total_price'] = $item['quantity'] * $item['price_per_item'];
            }

            unset(
                $item['formatted_price_per_item'],
                $item['formatted_total_price'],
            );

            if ($item['id'] == null) {
                unset($item['id']);
                $result[] = $model::create($item);
            } else {
                $model::where('id', $item['id'])->update($item);
                $result[] = $model::where('id', $item['id'])->first();
            }
        }

        return $result;
    }

    /**
     * Save request
     *
     * @param $id
     * @param Request $request
     * @return void
     */
    public function save($orderId, Request $request)
    {
        $purchase = $request->purchase_request;
        $expenditure = $request->expenditure_request;
        $payroll = $request->payroll_request;

        $id = [
            'purchase' => [],
            'expenditure' => [],
            'payroll' => [],
        ];

        $id = $this->getIdList($id, $purchase, 'purchase');
        $id = $this->getIdList($id, $expenditure, 'expenditure');
        $id = $this->getIdList($id, $payroll, 'payroll');

        DB::beginTransaction();

        try {
            $this->deleteRequest($id['purchase'], $orderId, ModelRequest::class, 'PURCHASE');
            $this->deleteRequest($id['expenditure'], $orderId, ModelRequest::class, 'EXPENDITURE');
            $this->deleteRequest($id['payroll'], $orderId, ModelRequest::class, 'PAYROLL');

            $purchase = $this->upsert($purchase, ModelRequest::class);
            $expenditure = $this->upsert($expenditure, ModelRequest::class);
            $payroll = $this->upsert($payroll, ModelRequest::class);

            DB::commit();

            return Response::instance()
                ->json([
                    'purchase' => $purchase,
                    'expenditure' => $expenditure,
                    'payroll' => $payroll,
                ])->success();

        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Delete request
     *
     * @param [type] $orderId
     * @return void
     */
    public function delete($orderId)
    {
        DB::beginTransaction();

        try {
            $deleted = ModelRequest::where('order_id', $orderId)->delete();

            DB::commit();

            return Response::instance()
                ->json([
                    'total_deleted' => $deleted
                ])->success();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
