<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use App\Model\Order;
use App\Model\PayrollRequest;
use App\Model\Request as ModelRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Get new orders only
     *
     * @return void
     */
    public function list(Request $request)
    {
        $status = strtoupper($request->get('status'));

        if ($status) {
            $orders = Order::where('status', $status)
                ->orderBy('order_time', 'desc')
                ->get();
        } else {
            $orders = Order::orderBy('order_time', 'desc')
                ->get();
        }

        foreach ($orders as $order) {
            $order->detail = json_decode($order->detail);
        }

        return Response::instance()
            ->json($orders)
            ->success();
    }

    /**
     * Get order detail
     *
     * @param int $id
     * @return void
     */
    public function get($id)
    {
        $order = Order::getOrFail(['id' => $id]);

        $order->detail = json_decode($order->detail);
        $order->order_detail = json_decode($order->order_detail);

        $purchaseRequest = ModelRequest::where([
            'order_id' => $id,
            'category' => ModelRequest::CATEGORY_PURCHASE
        ])->get();

        $expenditureRequest = ModelRequest::where([
            'order_id' => $id,
            'category' => ModelRequest::CATEGORY_EXPENDITURE
        ])->get();

        // todo payroll request
        $payrollRequest = PayrollRequest::where('order_id', $id)->get();

        $order['has_request'] = !$purchaseRequest->isEmpty() || !$expenditureRequest->isEmpty() || !$payrollRequest->isEmpty();

        return Response::instance()
            ->json($order)
            ->success();
    }

    /**
     * Process order. This will pass the order to production team
     *
     * @param int $id
     * @return void
     */
    public function process($id)
    {
        $order = Order::where('id', $id)
            ->update([
                'status' => Order::STATUS_PROCESSED
            ]);

        return Response::instance()
            ->json($order)
            ->success();
    }

    /**
     * Reject order
     *
     * @param int $id
     * @return void
     */
    public function reject(Request $request)
    {
        $order = Order::where('id', $request->id)
            ->update([
                'status' => Order::STATUS_REJECTED,
                'remark' => $request->remark
            ]);

        return Response::instance()
            ->json($order)
            ->success();
    }

    /**
     * Update order data
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $order = Order::where('id', $request->id)
            ->update($request->all());

        return Response::instance()
            ->json($order)
            ->success();
    }

    /**
     * Update order data
     *
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $order = Order::where('id', $id)
            ->delete();

        return Response::instance()
            ->json($order)
            ->success();
    }
}
