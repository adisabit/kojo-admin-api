<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Exceptions\HttpException;
use App\Library\Response;
use App\Model\ClothingButton;
use App\Model\ClothingKurRope;
use App\Model\ClothingMaterial;
use App\Model\ClothingPuring;
use App\Model\ClothingScreenPrinting;
use App\Model\ClothingStopper;
use App\Model\ClothingType;
use Illuminate\Http\Request;

class ClothingTypeController extends Controller
{
    /**
     * Get or fail
     *
     * @param string $model
     * @param $id
     * @param string $errorPrefix
     */
    private function getOrFail(string $model, $id, string $errorPrefix)
    {
        switch ( strtolower($model) ) {
            case 'clothing_button': 
                $data = ClothingButton::where('id', $id)->first();
                break;
            case 'clothing_kur_rope': 
                $data = ClothingKurRope::where('id', $id)->first();
                break;
            case 'clothing_material': 
                $data = ClothingMaterial::where('id', $id)->first();
                break;
            case 'clothing_puring': 
                $data = ClothingPuring::where('id', $id)->first();
                break;
            case 'clothing_screen_printing': 
                $data = ClothingScreenPrinting::where('id', $id)->first();
                break;
            case 'clothing_stopper': 
                $data = ClothingStopper::where('id', $id)->first();
                break;
            case 'clothing_type': 
                $data = ClothingType::where('id', $id)->first();
                break;
            case 'clothing_zipper': 
                $data = ClothingType::where('id', $id)->first();
                break;
        }

        if (!$data) {
            throw new DataNotFoundException("$errorPrefix tidak ditemukan");
        }

        return $data;
    }

    /**
     * Get all clothing types
     *
     * @return mixed
     */
    public function list()
    {
        $types = ClothingType::orderBy('name')->get();

        return Response::instance()
            ->json($types)
            ->success();
    }

    /**
     * Get clothing type detail
     *
     * @param int $id
     * @return mixed
     */
    public function detail($id)
    {
        $type = $this->getOrFail('clothing_type', $id, 'Jenis pakaian');

        return Response::instance()
            ->json($type)
            ->success();
    }

    /**
     * Delete type
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $type = ClothingType::where('id', $id)->delete();

        return Response::instance()
            ->json(['total_deleted' => $type])
            ->success();
    }

    /**
     * Update type
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $type = ClothingType::where('id', $request->id)
            ->update($request->all());

        return Response::instance()
            ->json(['total_updated' => $type])
            ->success();
    }

    /**
     * Create type
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $type = ClothingType::create($request->all());

        return Response::instance()
            ->json($type)
            ->success(201);
    }

    /**
     * Attach material
     *
     * @param mixed $typeId
     * @param mixed $materialId
     * @return void
     */
    public function attachMaterial($typeId, $materialId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $material = $this->getOrFail('clothing_material', $materialId, 'Bahan pakaian');

        $pivot = $type->materials()->where('material_id', $material->id)->first();

        if ($pivot) {
            throw new HttpException('Bahan pakaian telah di-attach!');
        }

        $type->materials()->attach($material->id);

        return Response::instance()
            ->json('Bahan pakaian berhasil di-attach!')
            ->success();
    }

    /**
     * Detach material
     *
     * @param mixed $typeId
     * @param mixed $materialId
     * @return void
     */
    public function detachMaterial($typeId, $materialId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $material = $this->getOrFail('clothing_material', $materialId, 'Bahan pakaian');

        $type->materials()->detach($material->id);

        return Response::instance()
            ->json('Bahan pakaian berhasil di-detach!')
            ->success();
    }

    /**
     * Attach button
     *
     * @param mixed $typeId
     * @param mixed $buttonId
     * @return void
     */
    public function attachButton($typeId, $buttonId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $button = $this->getOrFail('clothing_button', $buttonId, 'Kancing');

        $pivot = $type->buttons()->where('button_id', $button->id)->first();

        if ($pivot) {
            throw new HttpException('Kancing telah di-attach!');
        }

        $type->buttons()->attach($button->id);

        return Response::instance()
            ->json('Kancing berhasil di-attach!')
            ->success();
    }

    /**
     * Detach button
     *
     * @param mixed $typeId
     * @param mixed $buttonId
     * @return void
     */
    public function detachButton($typeId, $buttonId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $button = $this->getOrFail('clothing_button', $buttonId, 'Kancing');

        $type->buttons()->detach($button->id);

        return Response::instance()
            ->json('Kancing berhasil di-detach!')
            ->success();
    }

    /**
     * Attach kur rope
     *
     * @param mixed $typeId
     * @param mixed $kurRopeId
     * @return void
     */
    public function attachKurRope($typeId, $kurRopeId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $kurRope = $this->getOrFail('clothing_kur_rope', $kurRopeId, 'Tali kur');

        $pivot = $type->kurRopes()->where('kur_rope_id', $kurRope->id)->first();

        if ($pivot) {
            throw new HttpException('Tali kur telah di-attach!');
        }

        $type->kurRopes()->attach($kurRope->id);

        return Response::instance()
            ->json('Tali kur berhasil di-attach!')
            ->success();
    }

    /**
     * Detach kur rope
     *
     * @param mixed $typeId
     * @param mixed $kurRopeId
     * @return void
     */
    public function detachKurRope($typeId, $kurRopeId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $kurRope = $this->getOrFail('clothing_kur_rope', $kurRopeId, 'Tali kur');

        $type->kurRopes()->detach($kurRope->id);

        return Response::instance()
            ->json('Tali kur berhasil di-detach!')
            ->success();
    }

    /**
     * Attach puring
     *
     * @param mixed $typeId
     * @param mixed $puringId
     * @return void
     */
    public function attachPuring($typeId, $puringId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $puring = $this->getOrFail('clothing_puring', $puringId, 'Puring');

        $pivot = $type->purings()->where('puring_id', $puring->id)->first();

        if ($pivot) {
            throw new HttpException('Puring telah di-attach!');
        }

        $type->purings()->attach($puring->id);

        return Response::instance()
            ->json('Puring berhasil di-attach!')
            ->success();
    }

    /**
     * Detach puring
     *
     * @param mixed $typeId
     * @param mixed $puringId
     * @return void
     */
    public function detachPuring($typeId, $puringId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $puring = $this->getOrFail('clothing_puring', $puringId, 'Puring');

        $type->purings()->detach($puring->id);

        return Response::instance()
            ->json('Puring berhasil di-detach!')
            ->success();
    }

    /**
     * Attach stopper
     *
     * @param mixed $typeId
     * @param mixed $stopperId
     * @return void
     */
    public function attachStopper($typeId, $stopperId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $stopper = $this->getOrFail('clothing_stopper', $stopperId, 'Stoper');

        $pivot = $type->stoppers()->where('stopper_id', $stopper->id)->first();

        if ($pivot) {
            throw new HttpException('Stoper telah di-attach!');
        }

        $type->stoppers()->attach($stopper->id);

        return Response::instance()
            ->json('Stoper berhasil di-attach!')
            ->success();
    }

    /**
     * Detach stopper
     *
     * @param mixed $typeId
     * @param mixed $stopperId
     * @return void
     */
    public function detachStopper($typeId, $stopperId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $stopper = $this->getOrFail('clothing_stopper', $stopperId, 'Stoper');

        $type->stoppers()->detach($stopper->id);

        return Response::instance()
            ->json('Stoper berhasil di-detach!')
            ->success();
    }

    /**
     * Attach zipper
     *
     * @param mixed $typeId
     * @param mixed $zipperId
     * @return void
     */
    public function attachZipper($typeId, $zipperId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $zipper = $this->getOrFail('clothing_zipper', $zipperId, 'Seleting');

        $pivot = $type->zippers()->where('zipper_id', $zipper->id)->first();

        if ($pivot) {
            throw new HttpException('Seleting telah di-attach!');
        }

        $type->zippers()->attach($zipper->id);

        return Response::instance()
            ->json('Seleting berhasil di-attach!')
            ->success();
    }

    /**
     * Detach zipper
     *
     * @param mixed $typeId
     * @param mixed $zipperId
     * @return void
     */
    public function detachZipper($typeId, $zipperId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $zipper = $this->getOrFail('clothing_zipper', $zipperId, 'zipper');

        $type->zippers()->detach($zipper->id);

        return Response::instance()
            ->json('Seleting berhasil di-detach!')
            ->success();
    }

    /**
     * Attach screen printing
     *
     * @param mixed $typeId
     * @param mixed $screenPrintingId
     * @return void
     */
    public function attachScreenPrinting($typeId, $screenPrintingId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $screenPrinting = $this->getOrFail('clothing_screen_printing', $screenPrintingId, 'Sablon');

        $pivot = $type->screenPrintings()->where('screen_printing_id', $screenPrinting->id)->first();

        if ($pivot) {
            throw new HttpException('Sablon telah di-attach!');
        }

        $type->screenPrintings()->attach($screenPrinting->id);

        return Response::instance()
            ->json('Sablon berhasil di-attach!')
            ->success();
    }

    /**
     * Detach screen printing
     *
     * @param mixed $typeId
     * @param mixed $screenPrintingId
     * @return void
     */
    public function detachScreenPrinting($typeId, $screenPrintingId)
    {
        $type = $this->getOrFail('clothing_type', $typeId, 'Jenis pakaian');
        $screenPrinting = $this->getOrFail('clothing_screen_printing', $screenPrintingId, 'zipper');

        $type->screenPrintings()->detach($screenPrinting->id);

        return Response::instance()
            ->json('Sablon berhasil di-detach!')
            ->success();
    }
}
