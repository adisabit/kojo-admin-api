<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Helper\DisbursementHelper;
use App\Library\Response;
use App\Model\CashDisbursement;
use App\Model\CashDisbursementDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CashDisbursementController extends Controller
{
    /**
     * Create cash disbursement
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        DB::beginTransaction();

        $cashDisbursementCode = DisbursementHelper::generateCashDisbursementCode($request->disbursement_for, $request->date);

        try {
            // Create cash disbursement
            $cashDisbursement = CashDisbursement::create([
                'request_id' => $request->request_id,
                'code' => $cashDisbursementCode,
                'date' => $request->date ?? date("Y-m-d")
            ]);

            // Create cash disbursesment details
            foreach ($request->details as $detail) {
                $cashDisbursement->details()->create($detail);
            }

            // Load the detail for response
            $cashDisbursement->load('details');

            DB::commit();

            return Response::instance()
                ->json($cashDisbursement)
                ->success(201);
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error($e instanceof HttpException ? $e->getCode() : 500);
        }
    }

    /**
     * Get list of cash disbursement
     *
     * @return void
     */
    public function list(Request $request)
    {
        $category = strtoupper($request->category);

        if (!$category) {
            $cashDisbursement = CashDisbursement::all();
        } else {
            $cashDisbursement = CashDisbursement::whereHas('request', function($query) use ($category) {
                $query->where('category', $category);
            })->get();
        }

        return Response::instance()
            ->json($cashDisbursement)
            ->success();
    }

    /**
     * Retrieve cash disbursement
     *
     * @param mixed $id
     * @return void
     */
    public function retrieve($id)
    {
        $cashDisbursement = CashDisbursement::with(['details', 'request'])->where('id', $id)->first();

        if (!$cashDisbursement) {
            throw new DataNotFoundException('Cash disbursement tidak ditemukan');
        }

        return Response::instance()
            ->json($cashDisbursement)
            ->success();
    }

    /**
     * Update cash disbursement
     *
     * @param Request $request
     * @param mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $cashDisbursement = CashDisbursement::where('id', $id)->first();

        if (!$cashDisbursement) {
            throw new DataNotFoundException('Cash disbursement tidak ditemukan');
        }

        DB::beginTransaction();


        try {
            // Update cash disbursement
            $result = $cashDisbursement->update([
                'request_id' => $request->request_id,
                'date' => $request->date ?? date("Y-m-d")
            ]);

            // Update cash disbursesment details
            foreach ($request->details as $detail) {
                CashDisbursementDetail::where('id', $detail['id'])->update($detail);
            }

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error($e instanceof HttpException ? $e->getCode() : 500);
        }
    }

    /**
     * Delete cash disbursement
     *
     * @param mixed $id
     * @return void
     */
    public function delete($id)
    {
        $cashDisbursement = CashDisbursement::where('id', $id)->first();

        if (!$cashDisbursement) {
            throw new DataNotFoundException('Cash disbursement tidak ditemukan');
        }

        DB::beginTransaction();

        try {
            // delete cash disbursement detail
            $cashDisbursement->details()->delete();

            // delete cash disbursement
            $result = $cashDisbursement->delete();

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error($e instanceof HttpException ? $e->getCode() : 500);
        }
    }
}
