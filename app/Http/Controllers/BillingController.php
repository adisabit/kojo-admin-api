<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use App\Model\Billing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BillingController extends Controller
{
    /**
     * Create billing
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $billing = Billing::create($request->all());

        return Response::instance()
            ->json($billing)
            ->success(201);
    }

    /**
     * Retrieve billing
     *
     * @param mixed $id
     * @return void
     */
    public function retrieve($id)
    {
        $billing = Billing::with(['invoice', 'dpReceipt', 'paidOffReceipt'])
            ->where('id', $id)
            ->first();

        if (!$billing) {
            throw new DataNotFoundException('Billing tidak ditemukan!');
        }

        return Response::instance()
            ->json($billing)
            ->success();
    }

    /**
     * Update billing
     *
     * @param Request $request
     * @param mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $billing = Billing::where('id', $id)->first();

        if (!$billing) {
            throw new DataNotFoundException('Billing tidak ditemukan!');
        }

        $result = $billing->update($request->all());

        return Response::instance()
            ->json($result)
            ->success();
    }

    /**
     * Delete billing
     *
     * @param mixed $id
     * @return void
     */
    public function delete($id)
    {
        $billing = Billing::where('id', $id)->first();

        if (!$billing) {
            throw new DataNotFoundException('Billing tidak ditemukan!');
        }

        $result = $billing->delete();

        return Response::instance()
            ->json($result)
            ->success();
    }
}
