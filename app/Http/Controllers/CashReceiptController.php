<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Helper\ReceiptHelper;
use App\Library\Response;
use App\Model\CashReceipt;
use App\Model\Order;
use Illuminate\Http\Request;

class CashReceiptController extends Controller
{
    /**
     * Create create cash receipt.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $order = Order::where('id', $request->order_id)->first();

        if (!$order) {
            throw new DataNotFoundException('Order tidak ditemukan!');
        }

        switch ($request->type) {
            case CashReceipt::TYPE_ON_BANK:
                $cashReceiptCode = ReceiptHelper::generateCashReceiptCode('BANK', $request->receipt_for, $order->order_ref);
                break;
            case CashReceipt::TYPE_ON_HAND:
                $cashReceiptCode = ReceiptHelper::generateCashReceiptCode('CASH', $request->receipt_for, $order->order_ref);
        }

        $data = array_merge($request->all(), [
            'code' => $cashReceiptCode
        ]);

        $cashReceipt = CashReceipt::create($data);

        return Response::instance()
            ->json($cashReceipt)
            ->success();
    }

    /**
     * Get list of cash receipts
     *
     * @return void
     */
    public function list()
    {
        $receipts = CashReceipt::with('order')->get();

        return Response::instance()
            ->json($receipts)
            ->success();
    }

    /**
     * Retrieve cash receipt
     *
     * @param mixed $code
     * @return void
     */
    public function retrieve($code)
    {
        $cashReceipt = CashReceipt::with('order')->where('code', $code)->first();

        if (!$cashReceipt) {
            throw new DataNotFoundException('Cash receipt tidak ditemukan!');
        }

        return Response::instance()
            ->json($cashReceipt)
            ->success();
    }

    /**
     * Update cash receipt
     *
     * @param Request $request
     * @param [type] $code
     * @return void
     */
    public function update(Request $request, $code)
    {
        $cashReceipt = CashReceipt::where('code', $code)->first();

        if (!$cashReceipt) {
            throw new DataNotFoundException('Cash receipt tidak ditemukan!');
        }

        $order = Order::where('id', $request->order_id)->first();

        if (!$order) {
            throw new DataNotFoundException('Order tidak ditemukan!');
        }

        $result = $cashReceipt->update($request->all());

        return Response::instance()
            ->json($result)
            ->success();
    }

    /**
     * Delete cash receipt
     *
     * @param mixed $code
     * @return void
     */
    public function delete($code)
    {
        $cashReceipt = CashReceipt::where('code', $code)->first();

        if (!$cashReceipt) {
            throw new DataNotFoundException('Cash receipt tidak ditemukan!');
        }

        $result = $cashReceipt->delete();

        return Response::instance()
            ->json($result)
            ->success();
    }
}
