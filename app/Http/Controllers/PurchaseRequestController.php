<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Helper\RequestHelper;
use App\Library\Response;
use App\Model\CashDisbursement;
use App\Model\Request as ModelRequest;
use App\Model\RequestDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseRequestController extends Controller
{
    /**
     * Create new purchase request
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        DB::beginTransaction();

        // Genereate purchase request code
        $purchaseRequestCode = RequestHelper::generatePurchaseRequestCode($request->date);

        try {

            // Create purchase request
            $purchaseRequest = ModelRequest::create([
                'category'      => ModelRequest::CATEGORY_PURCHASE,
                'request_code'  => $purchaseRequestCode,
                'date'          => $request->date ?? date("Y-m-d H:i:s")
            ]);

            // Create purchase request detail
            foreach ($request->details as $detail) {
                $data = array_merge($detail, [
                    'price_per_unit' => (int) $detail['total_price'] / (int) $detail['amount']
                ]);

                $purchaseRequest->details()->create($data);
            }

            DB::commit();

            $purchaseRequest->load('details');

            return Response::instance()
                ->json($purchaseRequest)
                ->success(201);

        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error();
        }
    }

    /**
     * Retrieve list of purchase request
     *
     * @return void
     */
    public function list()
    {
        $purchaseRequest = ModelRequest::with('details')->where([
            'category'  => ModelRequest::CATEGORY_PURCHASE
        ])->get();

        return Response::instance()
            ->json($purchaseRequest)
            ->success();
    }

    /**
     * Retrieve purchase request
     *
     * @param mixed $id
     * @return void
     */
    public function retrieve($id)
    {
        $purchaseRequest = ModelRequest::with('details')
            ->where([
                'id'        => $id,
                'category'  => ModelRequest::CATEGORY_PURCHASE
            ])
            ->first();

        if (!$purchaseRequest) {
            throw new DataNotFoundException('Purchase request tidak ditemukan!');
        }

        return Response::instance()
            ->json($purchaseRequest)
            ->success();
    }

    /**
     * Update purchase request
     *
     * @param Request $request
     * @param mixed $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $purchaseRequest = ModelRequest::where([
            'id'        => $id,
            'category'  => ModelRequest::CATEGORY_PURCHASE
        ])->first();

        if (!$purchaseRequest) {
            throw new DataNotFoundException('Purchase request tidak ditemukan!');
        }

        // Genereate purchase request code
        $purchaseRequestCode = RequestHelper::generatePurchaseRequestCode($request->date);

        DB::beginTransaction();

        try {
            // Update purchase request
            $result = $purchaseRequest->update([
                'date'          => $request->date ?? date("Y-m-d H:i:s"),
                'request_code'  => $purchaseRequestCode
            ]);

            // Update purchase request details
            foreach ($request->details as $detail) {
                $data = [
                    'material'          => $detail['material'],
                    'amount'            => $detail['amount'],
                    'vendor'            => $detail['vendor'],
                    'unit'              => $detail['unit'],
                    'total_price'       => $detail['total_price'],
                    'price_per_unit'    => (int) $detail['total_price'] / (int) $detail['amount']
                ];

                RequestDetail::where('id', $detail['id'])->update($data);
            }

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error();
        }
    }

    /**
     * Delete purchase request
     *
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        $purchaseRequest = ModelRequest::with('details')
            ->where([
                'id'        => $id,
                'category'  => ModelRequest::CATEGORY_PURCHASE
            ])
            ->first();

        if (!$purchaseRequest) {
            throw new DataNotFoundException('Purchase request tidak ditemukan!');
        }

        DB::beginTransaction();

        try {
            // Delete purchase request details
            $purchaseRequest->details()->delete();

            // Update purchase request
            $result = $purchaseRequest->delete();

            DB::commit();

            return Response::instance()
                ->json($result)
                ->success();
        } catch (\Exception $e) {
            DB::rollBack();

            return Response::instance()
                ->json($e)
                ->error();
        }
    }
}
