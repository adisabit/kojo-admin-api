<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use App\Model\ClothingMaterial;
use App\Model\ClothingType;
use Illuminate\Http\Request;

class ClothingPriceController extends Controller
{
    /**
     * Get clothing type
     *
     * @param $id
     * @return Object
     */
    private function getClothingType($clothingTypeId): Object
    {
        $type = ClothingType::with('clothingMaterials')->where('id', $clothingTypeId)->first();

        if (!$type) {
            throw new DataNotFoundException('Jenis pakaian tidak ditemukan!');
        }

        return $type;
    }

    /**
     * Get clothing material
     *
     * @param $id
     * @return Object
     */
    private function getClothingMaterial($clothingMaterialId): Object
    {
        $material = ClothingMaterial::where('id', $clothingMaterialId)->first();

        if (!$material) {
            throw new DataNotFoundException('Jenis pakaian tidak ditemukan!');
        }

        return $material;
    }

    /**
     * Get clothing material
     *
     * @param $type
     * @param $materialId
     * @return Object
     */
    private function getClothingMaterialByType($type, $clothingMaterialId): Object
    {
        $material = $type->clothingMaterials()
            ->where('clothing_material_id', $clothingMaterialId)
            ->first();

        if (!$material) {
            throw new DataNotFoundException('Harga pakaian tidak ditemukan!');
        }

        return $material;
    }

    /**
     * Get list of prices
     *
     * @return void
     */
    public function list()
    {
        $result = [];

        $types = ClothingType::with('clothingMaterials')
            // ->whereHas('clothingMaterials')
            ->get();

        foreach ($types as $type) {
            $tmp = [
                'clothing_type_id' => $type->id,
                'clothing_type_name' => $type->name
            ];
            
            foreach ($type->clothingMaterials as $material) {
                $tmp = array_merge($tmp, [
                    'clothing_material_id' => $material->id,
                    'clothing_material_name' => $material->name,
                    'price' => $material->pivot->price,
                    'formatted_price' => formatIdr($material->pivot->price)
                ]);

                $result[] = $tmp;
            }
        }

        return Response::instance()
            ->json($result)
            ->success();
    }

    /**
     * Get price detail
     *
     * @param int $clothingTypeId
     * @param int $clothingMaterialId
     * @return void
     */
    public function detail($clothingTypeId, $clothingMaterialId)
    {
        $type = $this->getClothingType($clothingTypeId);
        $material = $this->getClothingMaterialByType($type, $clothingMaterialId);

        $result = [
            'clothing_type_id' => $type->id,
            'clothing_type_name' => $type->name,
            'clothing_material_id' => $material->id,
            'clothing_material_name' => $material->name,
            'price' => $material->pivot->price,
            'formatted_price' => formatIdr($material->pivot->price),
        ];

        return Response::instance()
            ->json($result)
            ->success();
    }

    /**
     * Type list
     *
     * @return void
     */
    public function typeList()
    {
        $types = ClothingType::orderBy('name')->get(['id', 'name']);

        return Response::instance()
            ->json($types)
            ->success();
    }

    /**
     * Material list
     *
     * @return void
     */
    public function materialList()
    {
        $materials = ClothingMaterial::all(['id', 'name']);

        return Response::instance()
            ->json($materials)
            ->success();
    }

    /**
     * Create price.
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $type = $this->getClothingType($request->clothing_type_id);
        $this->getClothingMaterial($request->clothing_material_id);

        $type->clothingMaterials()->syncWithoutDetaching([
            $request->clothing_material_id => [ 'price' => $request->price ]
        ]);

        $result = $type->toArray();

        unset($result['clothing_materials']);

        $result['clothing_material'] = $type->clothingMaterials()->where('clothing_material_id', $request->clothing_material_id)
            ->first()
            ->toArray();
        
        return Response::instance()
            ->json($result)
            ->success();
    }

    /**
     * Update.
     *
     * @return void
     */
    public function update($clothingTypeId, $clothingMaterialId, Request $request)
    {
        $type = $this->getClothingType($clothingTypeId);
        $this->getClothingMaterialByType($type, $clothingMaterialId);

        // delete old data
        $type->clothingMaterials()->detach($clothingMaterialId);
        
        $type = $this->getClothingType($request->clothing_type_id);
        
        // insert new data
        $type->clothingMaterials()->syncWithoutDetaching([
            $request->clothing_material_id => [ 'price' => $request->price ]
        ]);

        return Response::instance()
            ->json(['total_udpated' => 1])
            ->success();
    }

    public function isPriceExist($clothingTypeId, $clothingMaterialId)
    {
        try {
            $type = $this->getClothingType($clothingTypeId);
            $this->getClothingMaterialByType($type, $clothingMaterialId);

            return Response::instance()
                ->json([
                    'is_price_exist' => true
                ])->success();
        } catch (DataNotFoundException $e) {
            return Response::instance()
                ->json([
                    'is_price_exist' => false
                ])->success();
        }
    }

    /**
     * Remove price
     *
     * @param Request $request
     * @return void
     */
    public function remove($clothingTypeId, $clothingMaterialId)
    {
        $totalRemoved = 0;

        list($isTypeAndMaterialExist, $type) = $this->isTypeAndMaterialExist($clothingTypeId, $clothingMaterialId);
        
        if ($isTypeAndMaterialExist) {
            $type->clothingMaterials()->detach($clothingMaterialId);

            $totalRemoved += 1;
        }

        return Response::instance()
            ->json(['total_removed' => $totalRemoved])
            ->success();
    }
}
