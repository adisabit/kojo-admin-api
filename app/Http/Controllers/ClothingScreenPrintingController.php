<?php

namespace App\Http\Controllers;

use App\Exceptions\DataNotFoundException;
use App\Library\Response;
use Illuminate\Http\Request;
use App\Model\ClothingScreenPrinting;

class ClothingScreenPrintingController extends Controller
{
    /**
     * Get all clothing materials
     *
     * @return mixed
     */
    public function list()
    {
        $materials = ClothingScreenPrinting::orderBy('name')->get();

        return Response::instance()
            ->json($materials)
            ->success();
    }

    /**
     * Get clothing material detail
     *
     * @param int $id
     * @return mixed
     */
    public function detail($id)
    {
        $material = ClothingScreenPrinting::where('id', $id)->first();

        if (!$material) {
            throw new DataNotFoundException('Sablon tidak ditemukan!');
        }

        return Response::instance()
            ->json($material)
            ->success();
    }

    /**
     * Delete materials
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $material = ClothingScreenPrinting::where('id', $id)->delete();

        return Response::instance()
            ->json(['total_deleted' => $material])
            ->success();
    }

    /**
     * Update materials
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request)
    {
        $material = ClothingScreenPrinting::where('id', $request->id)
            ->update($request->all());

        return Response::instance()
            ->json(['total_updated' => $material])
            ->success();
    }

    /**
     * Create materials
     *
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {
        $material = ClothingScreenPrinting::create($request->all());

        return Response::instance()
            ->json($material)
            ->success(201);
    }
}
