<?php

namespace App\Http\Controllers;

use App\Library\Response;
use App\Model\Order;
use App\Model\RequestRealization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RequestRealizationController extends Controller
{
    public function list()
    {
        $realizations = RequestRealization::distinct()
            ->orderBy('created_at', 'desc')
            ->get(['order_id']);

        $result = [];

        foreach ($realizations as $realization) {
            $result[] = [
                'id'      => $realization->order_id,
                'order_ref'     => $realization->order->order_ref,
                'order_code'    => $realization->order->order_code,
                'order_name'    => $realization->order->order_name
            ];
        }

        return Response::instance()
            ->json($result)
            ->success();
    }

    public function detail($orderId)
    {
        $requests = RequestRealization::where('order_id', $orderId)->get()->toArray();

        $purchase = array_filter($requests, function($request) {
            return $request['category'] == RequestRealization::CATEGORY_PURCHASE;
        });

        $expenditure = array_filter($requests, function($request) {
            return $request['category'] == RequestRealization::CATEGORY_EXPENDITURE;
        });

        $payroll = array_filter($requests, function($request) {
            return $request['category'] == RequestRealization::CATEGORY_PAYROLL;
        });

        return Response::instance()
            ->json([
                'purchase_request'      => array_values($purchase),
                'expenditure_request'   => array_values($expenditure),
                'payroll_request'       => array_values($payroll),
            ])->success();
    }

    public function getOrders(Request $request)
    {
        $orderId = $request->order_id;
        
        if (!$orderId) {
            $orders = Order::whereNotIn('id', function($query) {
                $requestTable = with(new RequestRealization())->getTable();
    
                $query->select(['order_id'])
                    ->from($requestTable)
                    ->distinct()
                    ->get();
            })->get();
        } else {
            $orders = Order::where('id', $orderId)->get();
        }

        return Response::instance()
            ->json($orders)
            ->success();
    }

    /**
     * Get list of id
     *
     * @param $id
     * @param $request
     * @param $name
     * @return void
     */
    private function getIdList($id, $request, $name)
    {
        foreach ($request as $item) {
            if ($item['id'] != null) {
                $id[$name][] = $item['id'];
            }
        }

        return $id;
    }

    /**
     * Delete request
     *
     * @param $id
     * @param $model
     * @return void
     */
    private function deleteRealization($id, $orderId, $model, $category = NULL)
    {
        if ($category) {
            return $model::where([
                'order_id' => $orderId,
                'category' => $category
            ])->whereNotIn('id', $id)->delete();
        }

        return $model::whereNotIn('id', $id)->delete();
    }

    /**
     * Upsert
     *
     * @param $data
     * @param $model
     * @return array
     */
    private function upsert($data, $model)
    {
        $result = [];

        foreach ($data as $item) {
            if (!isset($item['total_price'])) {
                $item['total_price'] = $item['quantity'] * $item['price_per_item'];
            }

            unset(
                $item['formatted_price_per_item'],
                $item['formatted_total_price'],
            );

            if ($item['id'] == null) {
                unset($item['id']);
                $result[] = $model::create($item);
            } else {
                $model::where('id', $item['id'])->update($item);
                $result[] = $model::where('id', $item['id'])->first();
            }
        }

        return $result;
    }

    /**
     * Save request
     *
     * @param $id
     * @param Request $request
     * @return void
     */
    public function save($orderId, Request $request)
    {
        $purchase = $request->purchase_request;
        $expenditure = $request->expenditure_request;
        $payroll = $request->payroll_request;

        $id = [
            'purchase' => [],
            'expenditure' => [],
            'payroll' => [],
        ];

        $id = $this->getIdList($id, $purchase, 'purchase');
        $id = $this->getIdList($id, $expenditure, 'expenditure');
        $id = $this->getIdList($id, $payroll, 'payroll');

        DB::beginTransaction();

        try {
            $this->deleteRealization($id['purchase'], $orderId, RequestRealization::class, 'PURCHASE');
            $this->deleteRealization($id['expenditure'], $orderId, RequestRealization::class, 'EXPENDITURE');
            $this->deleteRealization($id['payroll'], $orderId, RequestRealization::class, 'PAYROLL');

            $purchase = $this->upsert($purchase, RequestRealization::class);
            $expenditure = $this->upsert($expenditure, RequestRealization::class);
            $payroll = $this->upsert($payroll, RequestRealization::class);

            DB::commit();

            return Response::instance()
                ->json([
                    'purchase' => $purchase,
                    'expenditure' => $expenditure,
                    'payroll' => $payroll,
                ])->success();

        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }

    /**
     * Delete request
     *
     * @param [type] $orderId
     * @return void
     */
    public function delete($orderId)
    {
        DB::beginTransaction();

        try {
            $deleted = RequestRealization::where('order_id', $orderId)->delete();

            DB::commit();

            return Response::instance()
                ->json([
                    'total_deleted' => $deleted
                ])->success();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }
    }
}
