<?php

namespace App\Http\Controllers;

use App\User;
use Firebase\JWT\JWT;
use App\Library\Response;
use Illuminate\Http\Request;
use App\Exceptions\AuthException;

class AuthController extends Controller
{
    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt",   // Issuer of the token
            'sub' => $user->email,  // Subject of the token
            'iat' => time(),        // Time when JWT was issued. 
            'exp' => time() + env('JWT_EXPIRY_TIME', 3600)  // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    
    /**
     * Do login
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $email = strtolower($request->email);

        $user = User::where('email', $email)->first(['email', 'name', 'password']);

        if (!$user) {
            throw new AuthException('Email / password tidak sesuai!');
        }

        $isPasswordMatch = app('hash')->check($request->password, $user->password);
        
        if (!$isPasswordMatch) {
            throw new AuthException('Email / password tidak sesuai!');
        }
        
        return Response::instance()
            ->json([
                'token' => $this->jwt($user)
            ])->success();
    }
}
