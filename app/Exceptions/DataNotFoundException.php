<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class DataNotFoundException extends HttpException
{
    /**
     * Data not found exception constructor
     *
     * @param $message
     * @param $code
     * @param Throwable $previous
     */
    public function __construct($message, $code = 204, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}