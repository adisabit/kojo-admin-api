<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class AuthException extends Exception
{
    /**
     * Data not found exception constructor
     *
     * @param $message
     * @param $code
     * @param Throwable $previous
     */
    public function __construct($message, $code = 401, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}