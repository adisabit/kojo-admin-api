<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class HttpException extends Exception
{
    /**
     * Http exception constructor
     *
     * @param $message
     * @param $code
     * @param Throwable $previous
     */
    public function __construct($message, $code = 400, Throwable $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}