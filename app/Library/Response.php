<?php

namespace App\Library;

use Exception;

class Response
{
    /**
     * Response instance
     *
     * @var Response
     */
    private static $instance;

    /**
     * Response payload
     *
     * @var mixed
     */
    private $payload;

    /**
     * Get response instance
     *
     * @return Response
     */
    public static function instance(): Response
    {
        if ( !isset(self::$instance) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Set json data
     *
     * @param $data
     * @return Response
     */
    public function json($data): Response
    {
        if ($data instanceof Exception) {       
            if (env('APP_DEBUG')) {
                $this->payload = [
                    'message' => $data->getMessage(),
                    'trace' => $data->getTraceAsString()
                ];
            } else {
                $this->payload = [
                    'message' => 'Internal Server Error'
                ];

            }
        } else {
            $this->payload = $data;
        }

        return $this;
    }

    /**
     * Return success response
     *
     * @return void
     */
    public function success($code = 200)
    {
        return response()->json([
            'success' => true,
            'status' => $code,
            'data' => $this->payload,
            'errors' => null
        ]);
    }

    /**
     * Return error response
     *
     * @return void
     */
    public function error($code = 500)
    {
        return response()->json([
            'success' => false,
            'status' => $code,
            'data' => null,
            'errors' => $this->payload
        ]);
    }
}