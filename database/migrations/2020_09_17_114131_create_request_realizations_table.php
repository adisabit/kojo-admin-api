<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestRealizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_realizations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id', false, true)->nullable();
            $table->string('category', 30)->nullable();
            $table->date('date')->nullable();
            $table->string('title', 100)->nullable();
            $table->string('vendor', 100)->nullable();
            $table->string('unit', 20)->nullable();
            $table->decimal('quantity', 10, 2, true)->nullable();
            $table->decimal('price_per_item', 20, 2, true)->nullable();
            $table->decimal('total_price', 20, 2, true)->nullable();
            $table->date('post_in_group_date')->nullable();
            $table->date('transaction_date')->nullable();
            $table->string('account_name', 100)->nullable();
            $table->string('account_number', 30)->nullable();
            $table->string('bank_name', 30)->nullable();
            $table->bigInteger('journal_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_realizations');
    }
}
