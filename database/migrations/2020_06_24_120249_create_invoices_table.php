<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id')->nullable();
            $table->string('code', 100)->nullable();
            $table->string('name', 100)->nullable();
            $table->text('address')->nullable();
            $table->string('phone_number', 30)->nullable();
            $table->string('type', 255)->nullable();
            $table->string('material', 255)->nullable();
            $table->integer('total')->nullable();
            $table->integer('unit_price')->nullable();
            $table->integer('total_price')->nullable();
            $table->text('detail')->nullable();
            $table->text('dp_proof_url')->nullable();
            $table->text('paid_off_proof_url')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
