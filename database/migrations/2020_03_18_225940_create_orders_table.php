<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_ref', 50);
            $table->string('name', 100);
            $table->text('address');
            $table->string('phone_number', 30);
            $table->string('type', 20);
            $table->string('material', 40);
            $table->integer('total');
            $table->text('detail');
            $table->date('due_date');
            $table->text('design_url');
            $table->boolean('is_dp_paid')->default(false);
            $table->boolean('is_paid_off')->default(false);
            $table->timestamp('order_time');
            $table->string('status', 20);
            $table->text('remark')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
