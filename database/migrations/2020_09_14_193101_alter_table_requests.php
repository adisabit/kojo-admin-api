<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requests', function(Blueprint $table) {
            $table->bigInteger('order_id', false, true)->nullable()->after('id');
            $table->string('title', 100)->nullable()->after('date');
            $table->string('vendor', 100)->nullable()->after('title');
            $table->string('unit', 20)->nullable()->after('title');
            $table->decimal('quantity', 10, 2, true)->nullable()->after('unit');
            $table->decimal('price_per_item', 20, 2, true)->nullable()->after('quantity');
            $table->decimal('total_price', 20, 2, true)->nullable()->after('price_per_item');
            $table->dropColumn(['request_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requests', function (Blueprint $table) {
            $table->dropColumn([
                'order_id', 'title', 'vendor', 'unit', 'quantity', 'price_per_item', 'total_price'
            ]);

            $table->string('request_code', 100)->nullable()->after('category');
        });
    }
}
