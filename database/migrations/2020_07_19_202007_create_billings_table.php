<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('invoice_id')->nullable();
            $table->string('dp_status', 30)->nullable()->comment('PAID / NOT_PAID_YET');
            $table->bigInteger('dp_receipt_id')->nullable();
            $table->string('paid_off_status', 30)->nullable()->comment('PAID / NOT_PAID_YET');
            $table->bigInteger('paid_off_receipt_id')->nullable();
            $table->date('dp_paid_date')->nullable();
            $table->date('paid_off_date')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
