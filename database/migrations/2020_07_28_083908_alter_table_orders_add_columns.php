<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableOrdersAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('order_code', 50)->nullable()->after('order_ref');
            $table->string('institution', 255)->nullable()->after('phone_number');
            $table->text('address_street')->nullable()->after('institution');
            $table->string('address_village', 100)->nullable()->after('address_street');
            $table->string('address_kecamatan', 100)->nullable()->after('address_village');
            $table->string('address_city', 100)->nullable()->after('address_kecamatan');
            $table->string('address_postal_code', 20)->nullable()->after('address_city');
            $table->string('address_province', 100)->nullable()->after('address_postal_code');
            $table->string('email', 255)->nullable()->after('address_province');
            $table->string('order_status', 255)->nullable()->after('email')->comment('FULL_ORDER / MAKLOON');
            $table->longText('order_detail')->nullable()->after('order_status')->comment('JSON object');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                'order_code',
                'institution',
                'address_street',
                'address_village',
                'address_kecamatan',
                'address_city',
                'address_postal_code',
                'address_province',
                'email',
                'order_status',
                'order_detail',
            ]);
        });
    }
}
