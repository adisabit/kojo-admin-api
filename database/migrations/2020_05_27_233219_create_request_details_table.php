<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('request_id')->nullable();
            $table->string('material', 50)->nullable();
            $table->decimal('amount', 10, 2, true)->nullable();
            $table->string('vendor', 100)->nullable();
            $table->string('unit', 50)->nullable();
            $table->decimal('price_per_unit', 20, 2, true)->nullable();
            $table->decimal('total_price', 20, 2, true)->nullable();
            $table->string('disbursement_id', 100)->nullable();
            $table->string('account_name', 100)->nullable();
            $table->string('account_number', 30)->nullable();
            $table->string('bank_name', 100)->nullable();
            $table->date('purchase_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_details');
    }
}
