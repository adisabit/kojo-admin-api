<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobSheetDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_sheet_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('job_sheet_id')->nullable();
            $table->bigInteger('order_id')->nullable();
            $table->string('title', 100)->nullable();
            $table->date('production_due_date', 50)->nullable();
            $table->integer('percentage')->nullable();
            $table->string('status', 50)->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_sheet_details');
    }
}
