<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_receipts', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id')->nullable();
            $table->string('type', 30)->nullable()->comment('ON_BANK / ON_HAND');
            $table->string('code', 50)->nullable();
            $table->string('receipt_for', 10)->nullable()->comment('LL / DP / BILL / OTH');
            $table->date('date')->nullable();
            $table->decimal('amount', 20, 2, true)->nullable();
            $table->decimal('total_amount', 20, 2, true)->nullable(); 
            $table->string('received_by', 100)->nullable(); 
            $table->text('remark')->nullable(); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_receipts');
    }
}
