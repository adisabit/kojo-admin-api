<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewUserFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = "CREATE OR REPLACE
            ALGORITHM = UNDEFINED VIEW `view_user_features` AS
            select
                `ku`.`name` AS `name`,
                `ku`.`email` AS `email`,
                `km`.`name` AS `module_name`,
                `kf`.`name` AS `feature_name`,
                `kfr`.`permissions` AS `feature_permissions`,
                `kf`.`route_name` AS `route_name`
            from
                (((((`users` `ku`
            join `role_user` `kru` on
                (`ku`.`id` = `kru`.`user_id`))
            join `roles` `kr` on
                (`kru`.`role_id` = `kr`.`id`))
            join `feature_role` `kfr` on
                (`kr`.`id` = `kfr`.`role_id`))
            join `features` `kf` on
                (`kfr`.`feature_id` = `kf`.`id`))
            join `modules` `km` on
                (`kf`.`module_id` = `km`.`id`))";

        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_user_features");
    }
}
