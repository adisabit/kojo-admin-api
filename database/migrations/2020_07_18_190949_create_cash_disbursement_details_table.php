<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashDisbursementDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_disbursement_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cash_disbursement_id')->nullable();
            $table->decimal('amount', 20, 2, true)->nullable();
            $table->string('type', 100)->nullable();
            $table->string('status', 100)->nullable();
            $table->longText('remark')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_disbursement_details');
    }
}
