<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableInvoicesAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function(Blueprint $table) {
            $table->dateTime('completed_time')->nullable();
            $table->string('order_code', 100)->nullable();
            $table->date('enter_date')->nullable();
            $table->date('exit_date')->nullable();
            $table->string('institution', 255)->nullable();
            $table->string('order_status', 30)->nullable();
            $table->string('payment_method', 30)->nullable();
            $table->boolean('is_paid_off')->nullable();
            $table->integer('delivery_fee', false, true)->nullable();
            $table->integer('price', false, true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function(Blueprint $table) {
            $table->dropColumn('completed_time');
            $table->dropColumn('order_code');
            $table->dropColumn('enter_date');
            $table->dropColumn('exit_date');
            $table->dropColumn('institution');
            $table->dropColumn('order_status');
            $table->dropColumn('payment_method');
            $table->dropColumn('is_paid_off');
            $table->dropColumn('delivery_fee');
            $table->dropColumn('price');
        });
    }
}

