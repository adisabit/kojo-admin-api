<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCashDisbursementDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_disbursement_details', function(Blueprint $table) {
            $table->string('transfer_to', 100)->nullable()->after('amount');
            $table->string('cash_from', 100)->nullable()->after('transfer_to');
            $table->string('bank_name', 100)->nullable()->after('cash_from');
            $table->string('account_number', 100)->nullable()->after('bank_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_disbursement_details', function(Blueprint $table) {
            $table->dropColumn([
                'transfer_to', 'cash_from', 'account_number', 'bank_name'
            ]);
        });
    }
}
