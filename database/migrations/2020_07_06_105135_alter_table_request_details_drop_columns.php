<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableRequestDetailsDropColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_details', function(Blueprint $table) {
            $table->dropColumn(['disbursement_id', 'account_name', 'account_number', 'bank_name', 'purchase_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_details', function(Blueprint $table) {
            $table->string('disbursement_id', 100)->nullable();
            $table->string('account_name', 100)->nullable();
            $table->string('account_number', 30)->nullable();
            $table->string('bank_name', 100)->nullable();
            $table->date('purchase_date')->nullable();
        });
    }
}
