<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_requests', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_id', false, true)->nullable();
            $table->string('employee_name', 100)->nullable();
            $table->string('employee_role', 30)->nullable();
            $table->decimal('quantity', 10, 2, true)->nullable();
            $table->decimal('price_per_item', 20, 2, true)->nullable();
            $table->decimal('total_price', 20, 2, true)->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_requests');
    }
}
