<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableOrdersSetNullables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('order_ref', 50)->nullable()->change();
            $table->string('name', 100)->nullable()->change();
            $table->text('address')->nullable()->change();
            $table->string('phone_number', 30)->nullable()->change();
            $table->string('type', 20)->nullable()->change();
            $table->string('material', 40)->nullable()->change();
            $table->integer('total')->nullable()->change();
            $table->text('detail')->nullable()->change();
            $table->date('due_date')->nullable()->change();
            $table->text('design_url')->nullable()->change();
            $table->string('status', 20)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('order_ref', 50)->change();
            $table->string('name', 100)->change();
            $table->text('address')->change();
            $table->string('phone_number', 30)->change();
            $table->string('type', 20)->change();
            $table->string('material', 40)->change();
            $table->integer('total')->change();
            $table->text('detail')->change();
            $table->date('due_date')->change();
            $table->text('design_url')->change();
            $table->string('status', 20)->change();
        });
    }
}
