<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// login routes
$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('login', ['uses' => 'AuthController@login']);
});

$router->group(['prefix' => 'admin'], function () use ($router) {

    // order module routes
    $router->group(['prefix' => 'order'], function () use ($router) {
        $router->get('/', ['uses' => 'OrderController@list']);
        $router->get('/{id}', ['uses' => 'OrderController@get']);
        $router->get('/{id}/request', ['uses' => 'RequestController@get']);
        $router->put('/{id}/request', ['uses' => 'RequestController@save']);
        $router->delete('/{id}/request', ['uses' => 'RequestController@delete']);
        $router->get('/{id}/request/realization', ['uses' => 'RequestController@get']);
        $router->put('/{id}/request/realization', ['uses' => 'RequestController@save']);
        $router->delete('/{id}/request/realization', ['uses' => 'RequestController@delete']);
        $router->put('/{id}/process', ['uses' => 'OrderController@process']);
        $router->put('/{id}/reject', ['uses' => 'OrderController@reject']);
        $router->put('/{id}', ['uses' => 'OrderController@update']);
        $router->delete('/{id}', ['uses' => 'OrderController@delete']);
    });

    // clothing module routes
    $router->group(['prefix' => 'clothing'], function () use ($router) {

        // material module routes
        $router->group(['prefix' => 'material'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingMaterialController@list']);
            $router->get('/{id}', ['uses' => 'ClothingMaterialController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingMaterialController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingMaterialController@update']);
            $router->post('/', ['uses' => 'ClothingMaterialController@create']);
        });

        // button module routes
        $router->group(['prefix' => 'button'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingButtonController@list']);
            $router->get('/{id}', ['uses' => 'ClothingButtonController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingButtonController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingButtonController@update']);
            $router->post('/', ['uses' => 'ClothingButtonController@create']);
        });

        // jacket type module routes
        $router->group(['prefix' => 'jacket_type'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingJacketTypeController@list']);
            $router->get('/{id}', ['uses' => 'ClothingJacketTypeController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingJacketTypeController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingJacketTypeController@update']);
            $router->post('/', ['uses' => 'ClothingJacketTypeController@create']);
        });

        // kur rope module routes
        $router->group(['prefix' => 'kur_rope'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingKurRopeController@list']);
            $router->get('/{id}', ['uses' => 'ClothingKurRopeController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingKurRopeController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingKurRopeController@update']);
            $router->post('/', ['uses' => 'ClothingKurRopeController@create']);
        });

        // puring module routes
        $router->group(['prefix' => 'puring'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingPuringController@list']);
            $router->get('/{id}', ['uses' => 'ClothingPuringController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingPuringController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingPuringController@update']);
            $router->post('/', ['uses' => 'ClothingPuringController@create']);
        });

        // screen printing module routes
        $router->group(['prefix' => 'screen_printing'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingScreenPrintingController@list']);
            $router->get('/{id}', ['uses' => 'ClothingScreenPrintingController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingScreenPrintingController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingScreenPrintingController@update']);
            $router->post('/', ['uses' => 'ClothingScreenPrintingController@create']);
        });

        // stopper module routes
        $router->group(['prefix' => 'stopper'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingStopperController@list']);
            $router->get('/{id}', ['uses' => 'ClothingStopperController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingStopperController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingStopperController@update']);
            $router->post('/', ['uses' => 'ClothingStopperController@create']);
        });

        // type module routes
        $router->group(['prefix' => 'type'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingTypeController@list']);
            $router->get('/{id}', ['uses' => 'ClothingTypeController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingTypeController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingTypeController@update']);
            $router->post('/', ['uses' => 'ClothingTypeController@create']);

            // attach routes
            $router->post("/{typeId}/material/{materialId}/attach", ['uses' => 'ClothingTypeController@attachMaterial']);
            $router->post("/{typeId}/button/{buttonId}/attach", ['uses' => 'ClothingTypeController@attachButton']);
            $router->post("/{typeId}/kur_rope/{kurRopeId}/attach", ['uses' => 'ClothingTypeController@attachKurRope']);
            $router->post("/{typeId}/puring/{puringId}/attach", ['uses' => 'ClothingTypeController@attachPuring']);
            $router->post("/{typeId}/screen_printing/{screenPrintingId}/attach", ['uses' => 'ClothingTypeController@attachScreenPrinting']);
            $router->post("/{typeId}/stopper/{stopperId}/attach", ['uses' => 'ClothingTypeController@attachStopper']);
            $router->post("/{typeId}/zipper/{zipperId}/attach", ['uses' => 'ClothingTypeController@attachZipper']);

            // detach routes
            $router->delete("/{typeId}/material/{materialId}/detach", ['uses' => 'ClothingTypeController@detachMaterial']);
            $router->delete("/{typeId}/button/{buttonId}/detach", ['uses' => 'ClothingTypeController@detachButton']);
            $router->delete("/{typeId}/kur_rope/{kurRopeId}/detach", ['uses' => 'ClothingTypeController@detachKurRope']);
            $router->delete("/{typeId}/puring/{puringId}/detach", ['uses' => 'ClothingTypeController@detachPuring']);
            $router->delete("/{typeId}/screen_printing/{screenPrintingId}/detach", ['uses' => 'ClothingTypeController@detachScreenPrinting']);
            $router->delete("/{typeId}/stopper/{stopperId}/detach", ['uses' => 'ClothingTypeController@detachStopper']);
            $router->delete("/{typeId}/zipper/{zipperId}/detach", ['uses' => 'ClothingTypeController@detachZipper']);

        });

        // zipper module routes
        $router->group(['prefix' => 'zipper'], function () use ($router) {
            $router->get('/', ['uses' => 'ClothingZipperController@list']);
            $router->get('/{id}', ['uses' => 'ClothingZipperController@detail']);
            $router->delete('/{id}', ['uses' => 'ClothingZipperController@delete']);
            $router->put('/{id}', ['uses' => 'ClothingZipperController@update']);
            $router->post('/', ['uses' => 'ClothingZipperController@create']);
        });

        // price module routes
        $router->group(['prefix' => 'price'], function () use ($router) {
            // clothing type and material map routes
            $router->get('/', ['uses' => 'ClothingPriceController@list']);
            $router->get('/type/{clothingTypeId}/material/{clothingMaterialId}/is_exist', ['uses' => 'ClothingPriceController@isPriceExist']);
            $router->get('/type', ['uses' => 'ClothingPriceController@typeList']);
            $router->get('/material', ['uses' => 'ClothingPriceController@materialList']);
            $router->get('/type/{clothingTypeId}/material/{clothingMaterialId}', ['uses' => 'ClothingPriceController@detail']);
            $router->put('/type/{clothingTypeId}/material/{clothingMaterialId}', ['uses' => 'ClothingPriceController@update']);
            $router->post('/', ['uses' => 'ClothingPriceController@create']);
            $router->delete('/type/{clothingTypeId}/material/{clothingMaterialId}', ['uses' => 'ClothingPriceController@remove']);
        });
    });

    // invoice module routes
    $router->group(['prefix' => 'invoice'], function () use ($router) {
        $router->get('/', ['uses' => 'InvoiceController@list']);
        $router->get('/{id}', ['uses' => 'InvoiceController@get']);
        $router->get('/{id}/download', ['uses' => 'InvoiceController@downloadNew']);
        $router->post('/save', ['uses' => 'InvoiceController@save']);
        $router->delete('/{id}', ['uses' => 'InvoiceController@delete']);
    });

    // request routes
    $router->group(['prefix' => 'request'], function () use ($router) {

        // request document routes
        $router->group(['prefix' => 'document'], function () use ($router) {
            $router->get('/', ['uses' => 'RequestController@list']);
            $router->get('/{orderId}', ['uses' => 'RequestController@detail']);
            $router->put('/{orderId}', ['uses' => 'RequestController@save']);
            $router->delete('/{orderId}', ['uses' => 'RequestController@delete']);
            $router->get('/orders/list', ['uses' => 'RequestController@getOrders']);
        });

        // request document routes
        $router->group(['prefix' => 'realization'], function () use ($router) {
            $router->get('/', ['uses' => 'RequestRealizationController@list']);
            $router->get('/{orderId}', ['uses' => 'RequestRealizationController@detail']);
            $router->put('/{orderId}', ['uses' => 'RequestRealizationController@save']);
            $router->delete('/{orderId}', ['uses' => 'RequestRealizationController@delete']);
            $router->get('/orders/list', ['uses' => 'RequestRealizationController@getOrders']);
        });
    });

    // disbursement routes
    $router->group(['prefix' => 'disbursement'], function () use ($router) {

        // cash disbursement routes
        $router->group(['prefix' => 'cash'], function () use ($router) {
            $router->get('/', ['uses' => 'CashDisbursementController@list']);
            $router->post('/', ['uses' => 'CashDisbursementController@create']);
            $router->get('/{id}', ['uses' => 'CashDisbursementController@retrieve']);
            $router->put('/{id}', ['uses' => 'CashDisbursementController@update']);
            $router->delete('/{id}', ['uses' => 'CashDisbursementController@delete']);
        });
    });

    // receipt routes
    $router->group(['prefix' => 'receipt'], function () use ($router) {

        // cash receipt routes
        $router->group(['prefix' => 'cash'], function () use ($router) {
            $router->get('/', ['uses' => 'CashReceiptController@list']);
            $router->post('/', ['uses' => 'CashReceiptController@create']);
            $router->get('/{code}', ['uses' => 'CashReceiptController@retrieve']);
            $router->put('/{code}', ['uses' => 'CashReceiptController@update']);
            $router->delete('/{code}', ['uses' => 'CashReceiptController@delete']);
        });
    });

    // billing routes
    $router->group(['prefix' => 'billing'], function () use ($router) {
        $router->post('/', ['uses' => 'BillingController@create']);
        $router->get('/{id}', ['uses' => 'BillingController@retrieve']);
        $router->put('/{id}', ['uses' => 'BillingController@update']);
        $router->delete('/{id}', ['uses' => 'BillingController@delete']);
    });

    // billing routes
    $router->group(['prefix' => 'job_sheet'], function () use ($router) {
        $router->get('/', ['uses' => 'JobSheetController@list']);
        $router->post('/', ['uses' => 'JobSheetController@create']);
        $router->get('/orders', ['uses' => 'JobSheetController@retrieveOrders']);
        $router->get('/{id}', ['uses' => 'JobSheetController@retrieve']);
        $router->put('/{id}', ['uses' => 'JobSheetController@update']);
        $router->delete('/{id}', ['uses' => 'JobSheetController@delete']);
        $router->delete('/{id}/detail/{detailId}', ['uses' => 'JobSheetController@deleteDetail']);
    });
});
